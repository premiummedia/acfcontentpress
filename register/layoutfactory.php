<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

namespace acfcontentpress\register;
defined( 'ABSPATH' ) or die();

use acfcontentpress\Config;
use acfcontentpress\register\RegisterHelper;
use acfcontentpress\register\FieldFactory;
use acfcontentpress\core\Layout;

class LayoutFactory{

    public static function createLayout($layoutName){
        $layoutBase = Config::getLayoutFolder();
        $layoutPath = path_join($layoutBase, $layoutName.".php");

        if( !file_exists($layoutPath) ){
            throw new \Exception( "Layout ".$layoutName." not found. Looking for: ".$layoutPath );
        }

        $layoutConfig = RegisterHelper::getConfigFromFile($layoutPath);

        $key = strtolower($layoutName);
        $label = ucfirst($layoutName);
        $layoutSettings = [];

        if( array_key_exists(\ACFCP::SETTINGS, $layoutConfig) ){
            $layoutSettings = $layoutConfig[\ACFCP::SETTINGS];
        }

        if( array_key_exists('label', $layoutConfig) ){
            $label = $layoutConfig['label'];
        }

        $layout = new Layout($key, $label, $layoutSettings);

        if( array_key_exists('fields', $layoutConfig) ){
            $fields = $layoutConfig['fields'];
            foreach( $fields as $field ){
                $layout->addField(FieldFactory::createField($field));
            }
        }

        return $layout;

    }

}
