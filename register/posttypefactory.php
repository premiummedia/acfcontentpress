<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

namespace acfcontentpress\register;
defined( 'ABSPATH' ) or die();

use acfcontentpress\Config;

class PostTypeFactory{

    public static function registerPostTypes( $postTypeConfigs ){
        foreach( $postTypeConfigs as $name => $settings ){
            self::registerPostType($name, $settings);
        }
    }

    public static function registerPostType($name, $settings = array() ){

        $cptSettings = array_merge(
            Config::$cptDefaultSettings,
            $settings
        );

        $cptObject = register_post_type(
            $name,
            $cptSettings
        );

        if( array_key_exists('names', $settings) ){
            do_action('acfcp/registered_post_type', $name, $settings['names']);
        }

        return $cptObject;

    }

}
