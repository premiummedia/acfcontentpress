<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

namespace acfcontentpress\register;
defined( 'ABSPATH' ) or die();

use acfcontentpress\register\FieldFactory;
use acfcontentpress\core\FieldGroup;


class FieldGroupFactory{

    /**
     * @param array $fieldGroupDefinition
     * [
     * 'appliesTo' => ['post', 'page-custom.php'],
     * 'fields' => [...],
     * 'process' => function($data, $id = null)
     * ]
     */
    public static function createFieldGroup($fieldGroupKey, $fieldGroupDefinition){

        $key = '';
        $label = '';
        $fieldGroupSettings = [];

        if( empty($fieldGroupKey) ){
            // How did that happen? We are not happy.
            throw new \Exception('Field Group Key cannot be empty.');
        }

        if( !is_array($fieldGroupDefinition) ){
            throw new \Exception( "Field Group Definition must be an array" );
        }


        if( array_key_exists(\ACFCP::SETTINGS, $fieldGroupDefinition) ){
            $fieldGroupSettings = $fieldGroupDefinition[\ACFCP::SETTINGS];
        }

        if( !array_key_exists('location', $fieldGroupSettings) ){
            $fieldGroupSettings['location'] = [];
        }

        if( !array_key_exists(\ACFCP::APPLIESTO, $fieldGroupDefinition) ){
            trigger_error("Field Group to registred does not contain ACFCP::APPLIESTO index. Field Group will therefore not be visible.");
        }else{
            $fieldGroupSettings['location'] = self::buildLocationValue($fieldGroupDefinition[\ACFCP::APPLIESTO]);
        }

        $key = strtolower($fieldGroupKey);

        if( array_key_exists('label', $fieldGroupSettings) ){
            $label = $fieldGroupSettings['label'];
        }else{
            $label = ucfirst($key);
        }

        $fieldGroup = new FieldGroup($key, $label, $fieldGroupSettings);

        if( array_key_exists('fields', $fieldGroupDefinition) ){
            $fields = $fieldGroupDefinition['fields'];
            foreach( $fields as $field ){
                $fieldGroup->addField(FieldFactory::createField($field));
            }
        }

        if( array_key_exists(\ACFCP::PROCESS, $fieldGroupDefinition) ){
            $fieldGroup->process = $fieldGroupDefinition[\ACFCP::PROCESS];
        }

        return $fieldGroup;

    }

    /*
     * @param array $appliesTo A list containing cpt and or page templates
     */
    public static function buildLocationValue($appliesTo){
        $location = [];
        foreach( $appliesTo as $cptOrTemplate ){
            $param = 'page_template';
            if( post_type_exists($cptOrTemplate) ){
                $param = 'post_type';
            }
            array_push($location, [
                [
                    'param' => $param,
                    'operator' => '==',
                    'value' => $cptOrTemplate
                ]
            ]);
        }
        return $location;
    }

}
