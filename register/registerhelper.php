<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

namespace acfcontentpress\register;
defined( 'ABSPATH' ) or die();

use acfcontentpress\Helpers;
use acfcontentpress\Config;

use acfcontentpress\register\FieldGroupFactory;

class RegisterHelper{

    public static function isNameValid($name){
        $reservedNames = array_merge(
            Config::$cptReservedNames,
            Config::$reservedTerms
        );

        if( in_array($name, $reservedNames) ){
            throw new \Exception("Name [".$name."] is reserved for WordPress internal use.");
        }else{
            return $name;
        }

    }

    public static function createValidNameFromFile($file){

        $fileName = pathinfo($file, PATHINFO_FILENAME);
        $nameSlug = Helpers::slug($fileName);
        $name = self::isNameValid($nameSlug);
        return $name;

    }

    public static function loadConfigClassNames($folder){

        return array_map(function($file){
            return self::createValidNameFromFile($file);
        }, self::getFiles($folder));

    }

    public static function loadConfigArrays($folder){

        $configArrays = array();

        foreach( self::getFiles($folder) as $file ){
            $name = self::createValidNameFromFile($file);
            $config = self::getConfigFromFile($folder."/".$file);
            $configArrays[$name] = $config;
        }

        return $configArrays;

    }

    public static function getFiles($folder){

        return array_filter(
            scandir($folder),
            function($folderContent) use ($folder){
                return !is_dir($folder."/".$folderContent) &&
                    $folderContent[0] !== '.' && $folderContent[0] !== '_';
            }
        );

    }

    public static function getConfigFromFile($file){

        if( file_exists($file) ){

            $config = require($file);

            if( !is_array( $config ) ){
                throw new \Exception("Config in file [".$file."] malformed.", 1);
            }
            return $config;
        }else{
            throw new \Exception("Trying to load config: File [".$file."] not found.", 1);
        }

    }


    public static function registerFieldGroups($fieldGroupArrays){

        $fieldGroups = [];

        foreach( $fieldGroupArrays as $fieldGroupKey => $fieldGroupDefinition){

            $fieldGroup = FieldGroupFactory::createFieldGroup(
                $fieldGroupKey, $fieldGroupDefinition
            );

            $fieldGroup->updateKeys();

            acf_add_local_field_group(
                $fieldGroup->getSettings()
            );

            $fieldGroups[$fieldGroup->getKey()] = $fieldGroup;

        }

    }

}
