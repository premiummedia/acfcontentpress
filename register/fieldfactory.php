<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

namespace acfcontentpress\register;
defined( 'ABSPATH' ) or die();

class FieldFactory{

    /**
     * @param array $fieldDefinition An array containing in succession: 1. Field [Class] 2. Field Key [String] 3. Field Label [String] (optional) 4. Field Settings [Array] (optional)
     */
    public static function createField($fieldDefinition){

        // check if $fieldDefinition is an array
        if( !is_array($fieldDefinition) || empty($fieldDefinition) ){
            throw new \Exception("Field Definition must be an array");
        }

        // Field Class should always be the first array value, if something else is here there will be an Exception on instantiation
        $fieldKlass = array_shift($fieldDefinition);

        // Check if there are subfields/layouts defined
        $subfields = [];
        $layouts = [];
        if( array_key_exists(\ACFCP::LAYOUTS, $fieldDefinition) ){
            $layouts = $fieldDefinition[\ACFCP::LAYOUTS];
            unset($fieldDefinition[\ACFCP::LAYOUTS]);
        }else if( array_key_exists(\ACFCP::FIELDS, $fieldDefinition) ){
            $subfields = $fieldDefinition[\ACFCP::FIELDS];
            unset($fieldDefinition[\ACFCP::FIELDS]);
        }


        // These values are needed for field creation and are extracted from the $fieldDefinition array.
        $key = "";
        $label = "";
        $settings = [];


        if( empty($fieldDefinition) ){
            throw new Exception("Field Definition must at least contain field type and field key");
        }

        // get the next array value, which should be the field key
        $key = array_shift($fieldDefinition);
        $key = strtolower($key);

        // allow for the label to be optional, so the next value will either be a label string or a settings array
        $labelOrSettings = null;

        if( !empty($fieldDefinition) ){
            $labelOrSettings = array_shift($fieldDefinition);
            if( is_array($labelOrSettings) ){
                $settings = $labelOrSettings;
                // label was omitted, create one from the field key
                $label = ucfirst($key);
            }else{
                $label = $labelOrSettings;
            }
        }

        // if the previos value was not the settings array, check if its the next one
        if( !empty($fieldDefinition) && empty($settings) ){
            $settingsIGuess = array_shift($fieldDefinition);
            if( is_array($settingsIGuess) ){
                $settings = $settingsIGuess;
            }
        }

        $field = null;

        try{
             $field = new $fieldKlass($key, $label, $settings);
        }catch( \Exception $e ){
            wp_die($e);
        }

        if( !empty($subfields) ){
            foreach($subfields as $subfieldDefinition){
                $field->addField(self::createField($subfieldDefinition));
            }
        }

        if( !empty($layouts) ){
            foreach($layouts as $layout){
                $field->addField(LayoutFactory::createLayout($layout));
            }
        }

        return $field;

    }

}
