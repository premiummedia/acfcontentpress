<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

namespace acfcontentpress\register;
defined( 'ABSPATH' ) or die();

class TaxonomyFactory{

    public static function registerTaxonomies( $taxonomyConfigs ){
        foreach( $taxonomyConfigs as $name => $settings ){
            self::registerTaxonomy($name, $settings);
        }
    }

    public static function registerTaxonomy($name, $settings){

        if( !array_key_exists('args', $settings) ||
            !array_key_exists('object_type', $settings) ){
                throw new \Exception("Taxonomy definition malformed. Missing args and/or object_type.");
        }

        register_taxonomy(
            $name,
            $settings['object_type'],
            $settings['args']
        );

        if( array_key_exists('names', $settings) ){
            do_action('acfcp/registered_taxonomy', $name, $settings['names']);
        }

        return true;

    }

}
