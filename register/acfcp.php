<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

use acfcontentpress\contrib\fields\CheckboxField;
use acfcontentpress\contrib\fields\ColorpickerField;
use acfcontentpress\contrib\fields\DatepickerField;
use acfcontentpress\contrib\fields\DatetimepickerField;
use acfcontentpress\contrib\fields\EmailField;
use acfcontentpress\contrib\fields\FileField;
use acfcontentpress\contrib\fields\FlexibleContentField;
use acfcontentpress\contrib\fields\GalleryField;
use acfcontentpress\contrib\fields\GeolocationField;
use acfcontentpress\contrib\fields\ImageField;
use acfcontentpress\contrib\fields\LinkField;
use acfcontentpress\contrib\fields\NumberField;
use acfcontentpress\contrib\fields\OembedField;
use acfcontentpress\contrib\fields\PageLinkField;
use acfcontentpress\contrib\fields\PasswordField;
use acfcontentpress\contrib\fields\PostObjectField;
use acfcontentpress\contrib\fields\RadioField;
use acfcontentpress\contrib\fields\RelationshipField;
use acfcontentpress\contrib\fields\RepeaterField;
use acfcontentpress\contrib\fields\SelectField;
use acfcontentpress\contrib\fields\TaxonomyField;
use acfcontentpress\contrib\fields\TextareaField;
use acfcontentpress\contrib\fields\TextField;
use acfcontentpress\contrib\fields\TrueFalseField;
use acfcontentpress\contrib\fields\UrlField;
use acfcontentpress\contrib\fields\UserField;
use acfcontentpress\contrib\fields\WysiwygField;

class ACFCP{


    const CHECKBOXFIELD = CheckboxField::class;
    const COLORPICKERFIELD = ColorpickerField::class;
    const DATEPICKERFIELD = DatepickerField::class;
    const DATETIMEPICKERFIELD = DatetimepickerField::class;
    const EMAILFIELD = EmailField::class;
    const FILEFIELD = FileField::class;
    const FLEXIBLECONTENTFIELD = FlexibleContentField::class;
    const GALLERYFIELD = GalleryField::class;
    const GEOLOCATIONFIELD = GeolocationField::class;
    const IMAGEFIELD = ImageField::class;
    const LINKFIELD = LinkField::class;
    const NUMBERFIELD = NumberField::class;
    const OEMBEDFIELD = OembedField::class;
    const PAGELINKFIELD = PageLinkField::class;
    const PASSWORDFIELD = PasswordField::class;
    const POSTOBJECTFIELD = PostObjectField::class;
    const RADIOFIELD = RadioField::class;
    const RELATIONSHIPFIELD = RelationshipField::class;
    const REPEATERFIELD = RepeaterField::class;
    const SELECTFIELD = SelectField::class;
    const TAXONOMYFIELD = TaxonomyField::class;
    const TEXTAREAFIELD = TextareaField::class;
    const TEXTFIELD = TextField::class;
    const TRUEFALSEFIELD = TrueFalseField::class;
    const URLFIELD = UrlField::class;
    const USERFIELD = UserField::class;
    const WYSIWYGFIELD = WysiwygField::class;


    const APPLIESTO = 'appliesTo';
    const FIELDS = 'fields';
    const LAYOUTS = 'layouts';
    const PROCESS = 'process';
    const SETTINGS = 'settings';


}
