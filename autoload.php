<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress;
defined( 'ABSPATH' ) or die();


spl_autoload_register(function($klass){
	if(
		strpos($klass, 'acfcontentpress') === 0 &&
		strpos($klass, 'acfcontentpressfrontend') === FALSE  &&
		strpos($klass, 'acfcontentpressi18n') === FALSE
		){
		$klass = str_replace('acfcontentpress', '', $klass);
		$klass = dirname(__FILE__) . '/' .str_replace('\\', '/', strtolower($klass)) . '.php';

		if( file_exists($klass) ){
			require_once $klass;
		}
	}
});

spl_autoload_register(function($klass){
	$types = array(
		'layouts',
		'fields',
		'fieldgroups'
	);
	$klass = strtolower($klass);
	$contents = get_template_directory().'/contents';

	foreach( $types as $type ){
		$file = $contents."/".$type."/".$klass.".php";
		if( file_exists($file) ){
			require_once $file;
		}
	}
});
