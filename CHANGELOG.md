# Change Log
All notable changes to this project will be documented in this file.

## [1.0.2] - 2017-12-12

### Changed
- Link to online docs in readme

## [1.0.1] - 2017-12-12

### Changed
- Correct license in composer.json

## [1.0.0] - 2017-12-8

### Added
- Docs
- License notice in source files and COPYING.txt
- Fields: Link Field and Color Picker Field

### Breaking Changes
- Changed the way to register Field Groups, Fields and Layouts to using arrays instead of classes
- Removed register.php, automatically register files in content folder.
- Removed ReplaceTitleSupport for now.
- Removed WP Filter for registering content classes (used for ACFCPI18N Field Groups)

### Changed
- Examples updated

## [0.5] - 2017-08-22

### Added

- ACF Components handle their data themselves
- Srcset generation for image fields

### Fixed

- Minor fixes

## [0.4.1] - 2017-08-07

### Added
- Added defined('ABSPATH') condition to prevent direct execution
- Error message when trying to use addField with something other than an object

### Fixed
- Proper namespace for exception thrown in ACFBase addField

## [0.4] - 2017-05-30

### Added
- Replace standard WordPress title functionality

## [0.3.1] - 2017-05-23

### Changed
- Check for reserved terms when registering custom post types and taxonomies

## [0.3.0] - 2017-05-23

### Added
- Register taxonomies and custom post types

### Changed
- Make sure the autoloader is specific to this plugin

## [0.2] - 2016-11-15

### Added
- Example content definition and registration

### Changed
- Autoloading independent of plugin directory name
- Add checking file_exists before including register.php
- Loading of ACF component classes now case insensitive

### Removed
- Language handling
