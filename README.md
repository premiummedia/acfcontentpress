# README #

### What's up? ###

* Create Advanced Custom Field Groups using php

See the documentation for more information.
https://acf-contentpress.readthedocs.io

### Installation ###

* Using composer or download and move to your plugin directory

NOTE: ACF is required for this to work. ACF PRO recommended to use Flexible Content Fields and other good stuff.

### Usage ###

1. Create a new directory `contents` in your active theme directory.
2. Add directories in `contents` for `fieldgroups` and `layouts` as needed.
3. Create a seperate file for each group/layout in their respective directories.

#### FieldGroups ####

Create field groups by creating a file in the `contents/fieldgroups` folder.

Eg. `contents/fieldgroups/headerfieldgroup.php`

```php
<?php

return [
    ACFCP::APPLIESTO => [
        'page'
    ],
    ACFCP::FIELDS => [
        [
            ACFCP::REPEATERFIELD,
            'slider',
            [
                'min' => 1,
                'button_label' => 'Add Image'
            ],
            ACFCP::FIELDS => [
                [
                    ACFCP::IMAGEFIELD,
                    'image'
                ],
                [
                    ACFCP::TEXTFIELD,
                    'caption',
                    'Image Caption',
                    [
                        'maxlength' => 64
                    ]
                ]
            ]
        ]
    ]
];
```

#### Layouts ####

Layouts are a collection of fields, which can be added to a flexible content field.
Create layouts by creating a file in the `contents/layouts` folder.

Eg. `contents/layouts/paragraphlayout.php`
```php
<?php

return [
    ACFCP::FIELDS => [
        [
            ACFCP::TEXTFIELD,
            'title',
        ],
        [
            ACFCP::TEXTAREAFIELD,
            'text'
        ],
    ],
    ACFCP::SETTINGS => [
        'label' => 'Paragraph'
    ]
];

```

Add the layout to a flexible content field: (within a field group field)

```php
[
    ACFCP::FLEXIBLECONTENTFIELD,
    'multicontent',
    ACFCP::LAYOUTS => [
        'paragraphlayout',
        'downloadlayout'
    ]
],
```
