<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\core;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\acfbase;
use acfcontentpress\core\FieldCollection;

class Layout extends ACFBase{

    protected $defaultSettings = array(
        'display' => 'block',
        'sub_fields' => array(),
        'min' => '',
        'max' => ''
    );

    public function __construct($name, $label = '', $settings = array()){
        parent::__construct($name, $label, $settings);

        $this->fields = new FieldCollection('sub_fields');

    }

    public function getProcessedData( $id, $data = null ){
        return "Layout Data";
        // There isn't a case that requires this to be called.
    }

}
