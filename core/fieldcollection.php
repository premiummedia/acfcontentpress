<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\core;
defined( 'ABSPATH' ) or die();

class FieldCollection implements \Iterator{
    protected $fields = array();
    private $index = '';
    protected $subFieldsName = '';

    public function __construct($subFieldsName){
        $this->subFieldsName = $subFieldsName;
    }

    public function getSubFieldsName(){
        return $this->subFieldsName;
    }

    public function addField($fieldName, $field){
        $this->fields[$fieldName] = $field;
        return $this->fields[$fieldName];
    }

    public function current(){
        return current($this->fields);
    }

    public function rewind(){
        reset($this->fields);
    }

    public function key(){
        return key($this->fields);
    }

    public function next(){
        next($this->fields);
    }

    public function valid(){
        return key($this->fields) != null;
    }

    public function flush(){
        $fields = $this->fields;
        $this->fields = array();
        return $fields;
    }

    public function get($key){
        if( array_key_exists($key, $this->fields) ){
            return $this->fields[$key];
        }
        return null;
    }

    public function keys(){
        return array_keys($this->fields);
    }

    public function getSettings(){
        return [
            $this->getSubFieldsName() => array_map(function($field){
                return $field->getSettings();
            }, $this->fields)
        ];
    }

}
