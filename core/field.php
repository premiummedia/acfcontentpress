<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\core;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\acfbase;

class Field extends ACFBase{

    protected $globalSettings = array(
        'instructions' => '',
        'required' => 0,
        'conditional_logic' => 0,
        'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => ''
        ),
        'default_value' => '',
        'acf_type' => 'field',
        'replacesTitle' => 0
    );

    public function __construct($key, $label = '', $settings = array() ){
        if( !empty($settings) && is_array($settings) && array_key_exists('replacesTitle', $settings) && $settings['replacesTitle'] ){
            if( !array_key_exists('wrapper', $settings) ){
                $settings['wrapper'] = $this->globalSettings['wrapper'];
            }
            if( !array_key_exists('class', $settings['wrapper']) ){
                $settings['wrapper']['class'] = "";
            }
            $settings['wrapper']['class'] .= 'replacesTitle';
        }

        parent::__construct($key, $label, $settings);
    }

    public function getProcessedData( $id ){
        return array(
            'type' => 'field',
            'data' => parent::getProcessedData( $id )
        );
    }

}
