<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\core;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\acfbase;
use acfcontentpress\core\FieldCollection;
use acfcontentpress\Helpers;

class FieldGroup extends ACFBase{

    protected $localSettings = array();

    protected $label = '';

    protected $defaultSettings = array(
        'key' => '',
        'title' => '',
        'fields' => array(),
        'location' => array(
            array(
    			array(
    				'param' => 'post_type',
    				'operator' => '==',
    				'value' => 'post',
    			)
    		)
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array(
            0 => 'the_content',
            1 => 'excerpt',
            2 => 'custom_fields',
            3 => 'discussion',
            4 => 'comments',
            //5 => 'revisions',
            6 => 'format',
            7 => 'tags',
            8 => 'send-trackbacks',
            9 => 'slug'
            //9 => 'permalink'
        ),
        'active' => 1,
        'description' => '',
        'acf_type' => 'fieldgroup'
    );

    public function __construct($key, $label, $settings = array()){

        parent::__construct(
            $key,
            $label,
            $settings
        );

        $this->fields = new FieldCollection('fields');

    }

    public function getProcessedData( $id ){

        $data = array();

        if( !empty( $this->fields ) ){
            foreach( $this->fields as $field ){
                $data[$field->getKey()] = $field->getProcessedData($id);
            }
        }

        return array(
            'type' => 'fieldgroup',
            'data' => $this->process($data, $id)
        );

    }

}
