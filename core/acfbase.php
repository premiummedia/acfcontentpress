<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\core;
defined( 'ABSPATH' ) or die();

use acfcontentpress\contrib\fields\EndPointTabField;
use acfcontentpress\contrib\fields\TabField;
use acfcontentpress\Config;
use acfcontentpress\Helpers;

/**
 * Base class for objects representing an acf entity.
 *
 * Mainly holds a settings array which is used to configure the entity.
 * Can hold sub-fields and update their keys.
 * Produces a settings array which is fed to the acf register function.
 *
 * @since 0.0.1
 */
class ACFBase{

    /**
     * Holds the definitive settings for this entity
     * @var array
     */
    protected $settings = array();

    /**
     * For use in child class: Holds settings relevant for all child classes.
     * @var array
     */
    protected $globalSettings = array();

    /**
     * For use in child class: Holds settings specific to the child class
     * @var array
     */
    protected $defaultSettings = array();

    /**
     * If this is a Field Group, Flexible Content Field or whatever, it might hold a collection of sub-fields.
     * @var FieldCollection
     */
    public $fields = false;

    /**
     * Sets name, label and merges default and global settings into $this->settings
     * @param string $name     Entity slug
     * @param string $label    Human-readable label
     * @param array  $settings Instance-specifig settings
     */
    public function __construct($name, $label = '', $settings = array()){

        // Entities are namespaced using a dot.
        if( strpos($name, '.') !== false ){
            throw new Exception('Do not use . in field name.');
        }

        if( empty($label) ){
            $label = ucfirst($name);
        }

        // merge all settings into $this->settings, settings in subclasses take precedence
        $this->settings = array_replace_recursive(
            $this->globalSettings,
            $this->defaultSettings,
            array(
                'label' => $label,
                'title' => $label
            ),
            $settings
        );

        $this->setKey(Helpers::slug($name));
    }

    /**
     * Updates the settings for this entity
     * @param string $key   Key to set $value for
     * @param mixed $value Value to be set for $key
     */
    public function set($key, $value){
        $this->settings[$key] = $value;
    }

    /**
     * ACF sometimes uses the key, sometimes the name to do stuff.
     * @param string $key Entity identifier
     */
    public function setKey($key){
        $this->settings['key'] = $this->settings['name'] = $key;
        return $this->settings['key'];
    }

    /**
     * Get the key for this entity
     * @return string The key
     */
    public function getKey(){
        if( array_key_exists('key', $this->settings) ){
            return $this->settings['key'];
        }else{
            throw new Exception('No Key defined');
        }
    }

    /**
     * Gets the settings of this and all of it's sub-entities.
     * @return array The settings array, ready for use in acf
     */
    public function getSettings(){
        $settings = $this->settings;
        if( $this->hasSubFields() ){
            $settings = array_merge(
                $settings, $this->fields->getSettings()
            );
        }
        return $settings;
    }

    /**
     * Get settings value by $key. Shorthand for getSetting($key)
     * @param  string $key Key to get value for
     * @return mixed The settings value for $key
     */
    public function get($key){
        return $this->getSetting($key);
    }

    /**
     * Get settings value by $key.
     * @param  string $key Key to get value for
     * @return mixed The settings value for $key
     */
    public function getSetting($key){
        if( array_key_exists($key, $this->settings) ){
            return $this->settings[$key];
        }else{
            throw new \Exception('Key '.$key. ' not found in $settings');
        }
    }

    /**
     * Check if this entity contains subfields
     * @return boolean True if it contains subfields
     */
    public function hasSubFields(){
        return boolval($this->fields);
    }

    public function addKeyToInstructions($key){
        if( array_key_exists('instructions', $this->settings) ){

            $instructions = $this->get('instructions');

            if( empty($instructions) ){
                $this->set('instructions', "(".$key.")");
            }else{
                $this->set('instructions', $instructions." (".$key.")");
            }

        }
    }

    public function updateKeys(){
        $this->updateKey();
    }

    public function updateKey($prefix = ''){

        $fieldKey = $this->getKey();
        $key = $fieldKey;

        if( !empty($prefix) ){
            $key = $prefix.".".$fieldKey;
        }

        $this->setKey($key);

        if( WP_DEBUG ){
            $this->addKeyToInstructions($key);
        }

        if( $this->hasSubFields() ){
            foreach( $this->fields as $subField ){
                $subField->updateKey($key);
            }
        }

    }

    public function addField($field){

        if( !is_object($field) ){
            throw new \Exception('FAIL: addField expects an object. Example Usage: $this->addField(new TextField(\'title\'))');
        }

        if( !$this->hasSubFields() ){
            throw new \Exception('FAIL: Trying to add subfield to object which can not contain subfields.');
        }

        return $this->fields->addField($field->getKey(), $field);

    }

    public function process($data, $id = null){

        if( property_exists($this, 'process') && $this->process instanceof \Closure ){
            return $this->process->__invoke($data, $id);
        }

        return $data;
    }

    public function getProcessedData( $id ){
        return $this->process( get_field( $this->getKey(), $id ), $id );
    }

}
