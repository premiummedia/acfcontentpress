<?php
/**
 * @package ACF ContentPress
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */

/*
Plugin Name: ACF ContentPress
Plugin URI: https://premiummedia.ch/acf-contentpress
Description: Allows for the easy creation of ACF Fields via PHP
Version: 1.0.2
Author: Andrin Heusser, Winterthur for premiummedia.ch
License: GPLv2 or later
Text Domain: acf-contentpress
*/
namespace acfcontentpress;
defined( 'ABSPATH' ) or die();


/* Set up namespace-based autoloading, as well as autoloading for user-defined contents (mainly ACF Layouts) */
require "autoload.php";

use acfcontentpress\Config;
use acfcontentpress\register\RegisterHelper;
use acfcontentpress\register\PostTypeFactory;
use acfcontentpress\register\TaxonomyFactory;

/* Create WP Admin Settings Page */
require_once "settings.php";

/* Provide global ACFCP class */
require_once "register/acfcp.php";

/* Wait for ACF to be loaded */
function ACFReady(){

	do_action('acfcp/beforeinit');

	/* Set up Google Maps Field */
	acf_update_setting(
		'google_api_key',
		Config::googleApiKey()
	);

	/* Register Custom Post Types */
	if( $postTypeFolder = Config::getPostTypeFolder() ){
		PostTypeFactory::registerPostTypes(
			RegisterHelper::loadConfigArrays($postTypeFolder)
		);
	}

	/* Register Taxonomies */
	if( $taxonomyFolder = Config::getTaxonomyFolder() ){
		TaxonomyFactory::registerTaxonomies(
			RegisterHelper::loadConfigArrays($taxonomyFolder)
		);
	}

	/* Register Field Groups */
	if( $fieldGroupFolder = Config::getFieldGroupFolder() ){
		RegisterHelper::registerFieldGroups(
			RegisterHelper::loadConfigArrays($fieldGroupFolder)
		);
	}

}

add_action('acf/init', 'acfcontentpress\ACFReady');
