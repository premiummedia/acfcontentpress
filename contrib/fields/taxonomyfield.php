<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\contrib\fields;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\field;

class TaxonomyField extends Field{
    protected $defaultSettings = array(
        'type' => 'taxonomy',
        /* (string) Specify the taxonomy to select terms from. Defaults to 'category' */
    	'taxonomy' => '',

    	/* (array) Specify the appearance of the taxonomy field. Defaults to 'checkbox'
    	Choices of 'checkbox' (Checkbox inputs), 'multi_select' (Select field - multiple), 'radio' (Radio inputs) or 'select' (Select field) */
    	'field_type' => 'checkbox',

    	/* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
    	'allow_null' => 0,

    	/* (bool) Allow selected terms to be saved as relatinoships to the post */
    	'load_save_terms' 	=> 0,

    	/* (string) Specify the type of value returned by get_field(). Defaults to 'id'.
    	Choices of 'object' (Term object) or 'id' (Term ID) */
    	'return_format'		=> 'id',

    	/* (bool) Allow new terms to be added via a popup window */
    	'add_term'			=> 1
    );
}
