<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\contrib\fields;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\field;

class TextareaField extends Field{
    protected $defaultSettings = array(
        'type' => 'textarea',

        /* (string) Appears within the input. Defaults to '' */
    	'placeholder' => '',

    	/* (string) Restricts the character limit. Defaults to '' */
    	'maxlength' => '',

    	/* (int) Restricts the number of rows and height. Defaults to '' */
    	'rows' => '',

    	/* (new_lines) Decides how to render new lines. Detauls to 'wpautop'.
    	Choices of 'wpautop' (Automatically add paragraphs), 'br' (Automatically add <br>) or '' (No Formatting) */
    	'new_lines' => '',

    	/* (bool) Makes the input readonly. Defaults to 0 */
    	'readonly' => 0,

    	/* (bool) Makes the input disabled. Defaults to 0 */
    	'disabled' => 0
    );
}
