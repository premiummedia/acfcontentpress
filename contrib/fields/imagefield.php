<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\contrib\fields;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\field;

class ImageField extends Field{
    protected $defaultSettings = array(
        'type' => 'image',
        /* (string) Specify the type of value returned by get_field(). Defaults to 'array'.
    	Choices of 'array' (Image Array), 'url' (Image URL) or 'id' (Image ID) */
    	'return_format' => 'array',

    	/* (string) Specify the image size shown when editing. Defaults to 'thumbnail'. */
    	'preview_size' => 'thumbnail',

    	/* (string) Restrict the image library. Defaults to 'all'.
    	Choices of 'all' (All Images) or 'uploadedTo' (Uploaded to post) */
    	'library' => 'all',

    	/* (int) Specify the minimum width in px required when uploading. Defaults to 0 */
    	'min_width' => 0,

    	/* (int) Specify the minimum height in px required when uploading. Defaults to 0 */
    	'min_height' => 0,

    	/* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0
    	The unit may also be included. eg. '256KB' */
    	'min_size' => 0,

    	/* (int) Specify the maximum width in px allowed when uploading. Defaults to 0 */
    	'max_width' => 0,

    	/* (int) Specify the maximum height in px allowed when uploading. Defaults to 0 */
    	'max_height' => 0,

    	/* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0
    	The unit may also be included. eg. '256KB' */
    	'max_size' => 0,

    	/* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
    	'mime_types' => ''
    );

    public function process($data, $id = null){
        if( is_array($data) && array_key_exists('url', $data) ){
            $sizes = array('large', 'medium_large', 'medium', 'thumbnail');
            $srcSet = $data['url']." ".$data['width'].'w, ';
            foreach($sizes as $size){
                $srcSet .= $data['sizes'][$size]." ".$data['sizes'][$size."-width"]."w,";
            }
            $data['srcset'] = $srcSet;
            return $data;
        }
    }
}
