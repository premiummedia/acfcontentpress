<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\contrib\fields;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\Field;
use acfcontentpress\Helpers;

class EndPointTabField extends Field{

    protected $defaultSettings = array(
        'type' => 'tab',
        'placement' => 'left',
        'endpoint' => 1
    );

    public function __construct(){

        $name = substr(md5(microtime()),rand(1,17),5);

        $this->settings = array_replace_recursive(
            $this->defaultSettings,
            $this->globalSettings,
            array(
                'label' => '',
                'title' => ''
            )
        );

        $this->setKey(Helpers::slug($name));
    }

}
