<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\contrib\fields;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\field;
use acfcontentpress\core\fieldcollection;

class FlexibleContentField extends Field{

    protected $defaultSettings = array(
        'type' => 'flexible_content',
        'button_label' => 'Add Row',
        'min' => '',
        'max' => '',
        'layouts' => array(

        ),
        'acf_type' => 'flex_field'
    );

    public function __construct($name, $label = '', $settings = array()){
        parent::__construct($name, $label, $settings);
        $this->fields = new FieldCollection('layouts');
    }

    public function getProcessedData( $id ){
        // echo "trying to load fc with key ".$this->getKey()." and id ".$id;
        // return array( $this->getKey() => get_field( $this->getKey(), $id ) );
        $data = get_field( $this->getKey(), $id );

        $return = array();

        if( $data && is_array($data) ){

            foreach( $data as $rawLayout ){
                if( array_key_exists('acf_fc_layout', $rawLayout) ){
                    $layout = $rawLayout['acf_fc_layout'];
                    $layoutData = \apply_filters('acfcp/layoutData', $rawLayout);
                    array_push($return, $layoutData);
                }
            }

        }

        return array(
            'type' => 'flex',
            'data' => $return
        );
    }

}
