<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\contrib\fields;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\field;

class FileField extends Field{
    protected $defaultSettings = array(
        'type' => 'file',
        /* (string) Specify the type of value returned by get_field(). Defaults to 'array'.
    	Choices of 'array' (File Array), 'url' (File URL) or 'id' (File ID) */
    	'return_format' => 'array',

    	/* (string) Specify the file size shown when editing. Defaults to 'thumbnail'. */
    	'preview_size' => 'thumbnail',

    	/* (string) Restrict the file library. Defaults to 'all'.
    	Choices of 'all' (All Files) or 'uploadedTo' (Uploaded to post) */
    	'library' => 'all',

    	/* (int) Specify the minimum filesize in MB required when uploading. Defaults to 0
    	The unit may also be included. eg. '256KB' */
    	'min_size' => 0,

    	/* (int) Specify the maximum filesize in MB in px allowed when uploading. Defaults to 0
    	The unit may also be included. eg. '256KB' */
    	'max_size' => 0,

    	/* (string) Comma separated list of file type extensions allowed when uploading. Defaults to '' */
    	'mime_types' => ''
    );
}
