<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\contrib\fields;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\field;
use acfcontentpress\core\FieldCollection;

class RepeaterField extends Field{

    protected $defaultSettings = array(
        'type' => 'repeater',
        'button_label' => 'Add Row',
        'min' => '',
        'max' => '',
        'layout' => 'row',
        'collapsed' => '',
        'sub_fields' => array(

        )
    );

    public function __construct($name, $label = '', $settings = array()){
        parent::__construct($name, $label, $settings);
        $this->fields = new FieldCollection('sub_fields');
    }

    public function getProcessedData( $id ){

        $data = get_field( $this->getKey(), $id );
        $data = \apply_filters('acfcp/layoutData', $data);

        return array(
            'type' => 'field',
            'data' => $this->process( $data, $id )
        );

    }

}
