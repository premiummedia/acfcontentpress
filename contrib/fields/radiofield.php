<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\contrib\fields;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\field;

class RadioField extends Field{
    protected $defaultSettings = array(
        'type' => 'radio',
        /* (array) Array of choices where the key ('red') is used as value and the value ('Red') is used as label */
    	'choices' => array(
    	),

    	/* (bool) Allow a custom choice to be entered via a text input */
    	'other_choice' => 0,

    	/* (bool) Allow the custom value to be added to this field's choices. Defaults to 0.
    	Will not work with PHP registered fields, only DB fields */
    	'save_other_choice' => 0,

    	/* (string) Specify the layout of the checkbox inputs. Defaults to 'vertical'.
    	Choices of 'vertical' or 'horizontal' */
    	'layout' => 0
    );
}
