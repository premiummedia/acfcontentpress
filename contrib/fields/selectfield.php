<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress\contrib\fields;
defined( 'ABSPATH' ) or die();

use acfcontentpress\core\field;

class SelectField extends Field{
    protected $defaultSettings = array(
        'type' => 'select',
        /* (array) Array of choices where the key ('red') is used as value and the value ('Red') is used as label */
    	'choices' => array(
    	),

        //@todo: add option array default_value

    	/* (bool) Allow a null (blank) value to be selected. Defaults to 0 */
    	'allow_null' => 0,

    	/* (bool) Allow mulitple choices to be selected. Defaults to 0 */
    	'multiple' => 0,

    	/* (bool) Use the select2 interfacte. Defaults to 0 */
    	'ui' => 0,

    	/* (bool) Load choices via AJAX. The ui setting must also be true for this to work. Defaults to 0 */
    	'ajax' => 0,

    	/* (string) Appears within the select2 input. Defaults to '' */
    	'placeholder' => ''
    );
}
