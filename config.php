<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
namespace acfcontentpress;
defined( 'ABSPATH' ) or die();

/**
 * Provides various application config values.
 */
class Config{

	/**
	 * Get full path to the content Directory
	 * @return string content directory path
	 */
	public static function getContentsPath(){
		return get_template_directory()."/contents/";
	}

	/**
	 * Returns the Google API Key from the plugin options page (if set)
	 * @return string the api key
	 */
	public static function googleApiKey(){
		return get_field('google_api_key', 'acfcp_options');
	}

	/**
	 * For an array of folder names, return the first folder that exists within $base
	 * @param  array $folderNames An array of folder names to search for
	 * @param  string $base        Base path to start looking from. Defaults to the contents folder
	 * @return string              Path to the first found folder
	 */
	public static function findViableFolder($folderNames, $base = ''){

		if( empty( $base ) ){
			$base = self::getContentsPath();
		}

		foreach( $folderNames as $folderName ){
			if( file_exists($base.$folderName) ){
				return $base.$folderName;
			}
		}
		// No folder found.
		return false;

	}

	/**
	 * Find the folder containing field group definitions
	 * @return string Full path to the folder containing field group definitions
	 */
	public static function getFieldGroupFolder(){
		$pathsToTry = array(
			"fieldgroups",
			"Fieldgroups",
			"FieldGroups"
		);

		return self::findViableFolder($pathsToTry);
	}

	/**
	 * Find the folder containing layout definitions
	 * @return string Full path to the folder containing layout definitions
	 */
    public static function getLayoutFolder(){
		$pathsToTry = array(
			"layouts",
			"Layouts"
		);

		return self::findViableFolder($pathsToTry);
	}

	/**
	 * Find the folder containing taxonomy definitions
	 * @return string Full path to the folder containing taxonomy definitions
	 */
	public static function getTaxonomyFolder(){
		$pathsToTry = array(
			"taxonomies",
			"Taxonomies",
			"taxonomy",
			"Taxonomy",
			"tax"
		);

		return self::findViableFolder($pathsToTry);
	}

	/**
	 * Find the folder containing custom post type definitions
	 * @return string Full path to the folder containing custom post type definitions
	 */
	public static function getPostTypeFolder(){
		$pathsToTry = array(
			"custom_post_types",
			"cpts",
			"customPostTypes",
			"CustomPostTypes"
		);
		return self::findViableFolder($pathsToTry);
	}

	/**
	 * An array of reserved names which might cause conflicts within wordpress
	 * @var array
	 */
	public static $cptReservedNames = array(
        'post',
        'page',
        'attachment',
        'revision',
        'nav_menu_item',
        'custom_css',
        'customize_changeset',
        'action',
        'author',
        'order',
        'theme'
    );

	/**
	 * An array of reserved names which might cause conflicts within wordpress
	 * @var array
	 */
	public static $reservedTerms = array(
		"attachment",
		"attachment_id",
		"author",
		"author_name",
		"calendar",
		"cat",
		"category",
		"category__and",
		"category__in",
		"category__not_in",
		"category_name",
		"comments_per_page",
		"comments_popup",
		"custom",
		"customize_messenger_channel",
		"customized",
		"cpage",
		"day",
		"debug",
		"embed",
		"error",
		"exact",
		"feed",
		"hour",
		"link_category",
		"m",
		"minute",
		"monthnum",
		"more",
		"name",
		"nav_menu",
		"nonce",
		"nopaging",
		"offset",
		"order",
		"orderby",
		"p",
		"page",
		"page_id",
		"paged",
		"pagename",
		"pb",
		"perm",
		"post",
		"post__in",
		"post__not_in",
		"post_format",
		"post_mime_type",
		"post_status",
		"post_tag",
		"post_type",
		"posts",
		"posts_per_archive_page",
		"posts_per_page",
		"preview",
		"robots",
		"s",
		"search",
		"second",
		"sentence",
		"showposts",
		"static",
		"subpost",
		"subpost_id",
		"tag",
		"tag__and",
		"tag__in",
		"tag__not_in",
		"tag_id",
		"tag_slug__and",
		"tag_slug__in",
		"taxonomy",
		"tb",
		"term",
		"terms",
		"theme",
		"title",
		"type",
		"w",
		"withcomments",
		"withoutcomments",
		"year"
	);

	/**
	 * Default settings for custom post types
	 * @var array
	 */
    public static $cptDefaultSettings = array(
        'public' => true
    );

	/**
	 * Default settings for custom taxonomies
	 * @var array
	 */
	public static $taxonomyDefaultSettings = array(
		'public' => true
	);

}
