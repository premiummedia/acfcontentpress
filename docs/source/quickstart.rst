Quickstart
##########

Installation
============

Using Composer:
::

    composer install premiummedia/acfcontentpress


Usage
=====

By default, ACFCP will look for a 'contents' directory in in your active theme folder. Inside this directory you add folders to separate your field group and layout definitions.

Recommended folder structure:
::

    YOUR_THEME_DIRECTORY
        > [WordPress files, index.php, style.css etc.]
        > contents
            > fieldgroups
                > ...
            > layouts
                > ...
            > cpts
                > ...
            > taxonomies
                > ...
