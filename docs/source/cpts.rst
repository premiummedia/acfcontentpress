Creating Custom Post Types
##########################

ACFCP will look for custom post types in this location:
::

    [YOUR_THEME_DIRECTORY]/contents/cpts/

Create a new file within this directory for your custom post type. The filename will be the slug of the post type.

Return an array, which corresponds to the second parameter of the standard-WordPress 'register_post_type' function.

'book.php'

.. code-block:: php

    <?php

    return [
        "label" => "Book",
        "public" => true,
        "has_archive" => false
    ];
