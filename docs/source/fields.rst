Creating Field Groups
#####################

ACFCP will look for field groups in this location:
::

    [YOUR_THEME_DIRECTORY]/contents/fieldgroups/

Create a new file within this directory for your field group. Best practice for naming is appending the 'fieldgroup' suffix. eg. 'bookfieldgroup.php' for holding fields to be attached to a book.

Within this file you need to return an array containing the field group definition.

'bookfieldgroup.php'

.. code-block:: php

    <?php

    return [
        ACFCP::APPLIESTO => [
            'book'
        ],
        ACFCP::FIELDS => [
            [
                ACFCP::IMAGEFIELD,
                'cover',
                'Cover Image'
            ],
            [
                ACFCP::TEXTFIELD,
                'isbn',
                'ISBN Number',
                [
                    'instructions' => 'The Book\'s unique ISBN Number',
                ]
            ],
            [
                ACFCP::TEXTAREAFIELD,
                'summary',
                [
                    'required' => true
                ]
            ],
        ],
        ACFCP::SETTINGS => [
            'style' => 'seamless'
        ]
    ];

The first array entry 'ACFCP::APPLIESTO' contains an array of locations, where this field group should be attached. Valid entries are (custom) post types and page templates.

The second entry 'ACFCP::FIELDS' contains an array of fields contained within this field group. See the next chapter for more details.

The last entry, 'ACFCP::SETTINGS', is optional. Here you may add additional settings for the field group. For available settings, see the ACF Documentation. Most used are probably:

- style: Either 'default' (box) or 'seamless' (no box)
- menu_order: 0..n, default 0, a number for ordering multiple field groups

Field Definition
================

Within the 'ACFCP::FIELDS' array entry, create a separate array for each field with this schema: (<> means optional)

.. code-block:: php

    [
        Field Type
        Field Key/Id (string)
        <Field Label> (string)
        <Field Settings> (array)
    ]

Besides the field type, the field key is always required. Keep this nice and short, it is for internal use only. For a human-readable version, set a field label.

If the field label is omitted, the field key is used as the label, with its first character uppercased.

Available Field Types
---------------------

See the ACF Website for more information on these fields.

.. code-block:: php

    ACFCP::CHECKBOXFIELD
    ACFCP::COLORPICKERFIELD
    ACFCP::DATEPICKERFIELD
    ACFCP::DATETIMEPICKERFIELD
    ACFCP::EMAILFIELD
    ACFCP::FILEFIELD
    ACFCP::FLEXIBLECONTENTFIELD
    ACFCP::GALLERYFIELD
    ACFCP::GEOLOCATIONFIELD
    ACFCP::IMAGEFIELD
    ACFCP::LINKFIELD
    ACFCP::NUMBERFIELD
    ACFCP::OEMBEDFIELD
    ACFCP::PAGELINKFIELD
    ACFCP::PASSWORDFIELD
    ACFCP::POSTOBJECTFIELD
    ACFCP::RADIOFIELD
    ACFCP::RELATIONSHIPFIELD
    ACFCP::REPEATERFIELD
    ACFCP::SELECTFIELD
    ACFCP::TAXONOMYFIELD
    ACFCP::TEXTAREAFIELD
    ACFCP::TEXTFIELD
    ACFCP::TRUEFALSEFIELD
    ACFCP::URLFIELD
    ACFCP::USERFIELD
    ACFCP::WYSIWYGFIELD

Available Field Settings
------------------------

Each field has different settings available. A text field has a placeholder, whereas an image field has a min-height, etc. Set these by adding an array to your field definition. See the ACF Website for available field settings.

Flexible Content Fields
=======================

Flexible content fields have layouts added to them, which themselves contain fields. To get started, first we have to create one or more layouts.

Layouts
-------

Layouts are defined pretty much the same as a field group: Create a file with an array definition in the direcotry:

.. code-block:: php

    [YOUR_THEME_DIRECTORY]/contents/fieldgroups/


Note that there is no need for a location setting, because we will be adding the layout directly to a flexible field. This allows you to easily use your layout in multiple flexible content fields at the same time.

'paragraphlayout.php'

.. code-block:: php

     <?php

     return [
         ACFCP::FIELDS => [
             [
                 ACFCP::TEXTFIELD,
                 'title',
             ],
             [
                 ACFCP::TEXTAREAFIELD,
                 'text'
             ],
         ],
         ACFCP::SETTINGS => [
             'label' => 'Paragraph'
         ]
     ];

Once we've defined a layout, we can add it to a flexible content field.

'mixedcontentfieldgroup.php'

.. code-block:: php

    <?php

    return [
        ACFCP::APPLIESTO => [
            'page'
        ],
        ACFCP::FIELDS => [
            [
                ACFCP::FLEXIBLECONTENTFIELD,
                'multicontent',
                'Multi Content',
                ACFCP::SETTINGS => [
                    'button_label' => 'Add Content'
                ],
                ACFCP::LAYOUTS => [
                    'paragraphlayout'
                ]
            ]
        ]
    ];

Repeater Fields
===============

For adding fields to a repeater fields, just add an array to the field definition. In this example, we create a layout containing a repeater field.

'downloadlayout.php'

.. code-block:: php

    <?php

    return [
        ACFCP::FIELDS => [
            [
                ACFCP::TEXTFIELD,
                'title',
            ],
            [
                ACFCP::REPEATERFIELD,
                'files',
                ACFCP::FIELDS => [
                    [
                        ACFCP::TEXTFIELD,
                        'name',
                        'Filename'
                    ],
                    [
                        ACFCP::FILEFIELD,
                        'file',
                        [
                            'required' => 1
                        ]
                    ]
                ]
            ]
        ],
        ACFCP::SETTINGS => [
            'label' => 'Download'
        ]
    ];
