Creating Taxonomies
###################

ACFCP will look for taxonomies in this location:
::

    [YOUR_THEME_DIRECTORY]/contents/taxonomies/

Create a new file within this directory for your taxonomy. The filename will be the slug of the taxonomy.

Return an array, which contains the keys 'object_type' and 'args'. They both correspond to the second, resp. third parameter of the standard-WordPress 'register_taxonomy' function.

'genre.php'

.. code-block:: php

    <?php

    return [
        "args" => [
            "public" => true,
            "labels" => [
                "name" => "Genres",
                "singular_name" => "Genre",
                "menu_name" => "Genre"
            ]
        ],
        "object_type" => ["book"]
    ];
