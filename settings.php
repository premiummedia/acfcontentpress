<?php
/**
 * ACF ContentPress (ACFCP)
 * Allows for the easy creation of ACF Fields via PHP
 *
 * Copyright (C) 2017 Andrin Heusser, Winterthur
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 */
defined( 'ABSPATH' ) or die();
add_action('acf/init', 'add_option_page_acfcp');

function add_option_page_acfcp(){

	if( function_exists( 'acf_add_options_page' ) ){
		acf_add_options_page(array(
			'page_title' => 'ACF ContentPress',
			'post_id' => 'acfcp_options'
		));

		acf_add_local_field_group(array (
			'key' => 'group_58189fdc4bebe',
			'title' => 'ACF ContentPress Options',
			'fields' => array (
				array (
					'key' => 'field_5818a0e61d6d6',
					'label' => 'Google API Key',
					'name' => 'google_api_key',
					'type' => 'text',
					'instructions' => 'Used by ACF\'s Google Map Field',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => '',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),
				/*array (
					'key' => 'field_5818a0e61d6d5',
					'label' => 'Contents Class File Location',
					'name' => 'content_register',
					'type' => 'text',
					'instructions' => 'Path from your active theme directory to the file that holds your Contents Class',
					'required' => 0,
					'conditional_logic' => 0,
					'wrapper' => array (
						'width' => '',
						'class' => '',
						'id' => '',
					),
					'default_value' => 'contents/register.php',
					'placeholder' => '',
					'prepend' => '',
					'append' => '',
					'maxlength' => '',
				),*/
			),
			'location' => array (
				array (
					array (
						'param' => 'options_page',
						'operator' => '==',
						'value' => 'acf-options-acf-contentpress',
					),
				),
			),
			'menu_order' => 0,
			'position' => 'normal',
			'style' => '',
			'label_placement' => 'top',
			'instruction_placement' => 'label',
			'hide_on_screen' => '',
			'active' => 1,
			'description' => '',
		));

	}

}


?>
