<?php

return [
    ACFCP::APPLIESTO => [
        'page'
    ],
    ACFCP::FIELDS => [
        [
            ACFCP::FLEXIBLECONTENTFIELD,
            'multicontent',
            ACFCP::LAYOUTS => [
                'paragraphlayout',
                'downloadlayout'
            ]
        ]
    ],
    ACFCP::SETTINGS => [
        'style' => 'seamless'
    ]
];
