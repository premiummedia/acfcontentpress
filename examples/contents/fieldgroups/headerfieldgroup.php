<?php

return [
    ACFCP::APPLIESTO => [
        'page'
    ],
    ACFCP::FIELDS => [
        [
            ACFCP::REPEATERFIELD,
            'slider',
            [
                'min' => 1,
                'button_label' => 'Add Image'
            ],
            ACFCP::FIELDS => [
                [
                    ACFCP::IMAGEFIELD,
                    'image'
                ],
                [
                    ACFCP::TEXTFIELD,
                    'caption',
                    'Image Caption',
                    [
                        'maxlength' => 64
                    ]
                ]
            ]
        ]
    ]
];
