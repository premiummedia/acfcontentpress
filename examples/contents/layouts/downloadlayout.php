<?php


return [
    ACFCP::FIELDS => [
        [
            ACFCP::TEXTFIELD,
            'title',
        ],
        [
            ACFCP::REPEATERFIELD,
            'files',
            ACFCP::FIELDS => [
                [
                    ACFCP::TEXTFIELD,
                    'name',
                    'Filename'
                ],
                [
                    ACFCP::FILEFIELD,
                    'file',
                    [
                        'required' => true
                    ]
                ]
            ]
        ]
    ],
    ACFCP::PROCESS => function($data, $id = null){
        $data['numDownloads'] = sizeof($data);
        return $data;
    },
    ACFCP::SETTINGS => [
        'label' => 'Download'
    ]
];
