<?php


return [
    ACFCP::FIELDS => [
        [
            ACFCP::TEXTFIELD,
            'title',
        ],
        [
            ACFCP::TEXTAREAFIELD,
            'text'
        ],
    ],
    ACFCP::SETTINGS => [
        'label' => 'Paragraph'
    ]
];
